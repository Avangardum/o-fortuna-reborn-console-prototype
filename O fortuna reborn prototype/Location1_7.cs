﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_7 : Location
    {
        bool bagNearBox = false;
        bool boxBlown = false;
        bool catRanAway = false;

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Сдуть пакет", BlowBag, true, true));
            actions.Add(new GameAction("Сдуть коробку", BlowBox, true, true));
        }

        public Location1_7()
        {
            ShowMessage("Коди и Тобиас идут по дороге. Рядом с дорогой сидит кошка. Неподалёку лежит пакет. Между кошкой и пакетом лежит картонная коробка");
            AskForActions();
            if (catRanAway)
            {
                ShowMessage("Тобиас и Коди проходят мимо");
                Level1.Instance.AddMinutes(5);
            }
            else
            {
                ShowMessage("Коди срывается и бежит за кошкой. Кошка убегает. Тобиас пытается поймать Коди, и через некоторое время это у него получается, и они идут дальше");
                Level1.Instance.AddMinutes(10);
            }
            new Location1_3();
        }

        void BlowBox()
        {
            boxBlown = true;
            if(bagNearBox)
            {
                ShowMessage("Подул ветер, и картонная коробка улетела вместе с пакетом");
            }
            else
            {
                ShowMessage("Подул ветер, и картонная коробка улетела");
            }
        }

        void BlowBag()
        {
            if(boxBlown)
            {
                catRanAway = true;
                ShowMessage("Пакет сдувает ветром, он попадает в кошку, кошка пугается и убегает");
            }
            else
            {
                bagNearBox = true;
                ShowMessage("Пакет сдувает ветром, он попадает в коробку и цепляется за неё");
            }
        }
    }
}
