﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class GameAction
    {
        public string Name { get; }
        public Action Action { get; }
        public bool IsActive { get; set; }

        public GameAction(string name, Action action, bool isActive, bool isOneOff)
        {
            Name = name;
            this.Action = action;
            IsActive = isActive;
            if (isOneOff)
                this.Action += delegate { IsActive = false; };
        }

        public void Invoke() => this.Action();
    }
}
