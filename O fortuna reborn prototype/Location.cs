﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace O_fortuna_reborn_prototype
{
    abstract class Location
    {
        protected IPresenter Presenter => Game.Presenter;

        protected void ShowMessage(string message) => Presenter.ShowMessage(message);

        protected void AskForActions() => Presenter.AskForActions(this);

        protected List<GameAction> actions = new List<GameAction>();

        public IEnumerable<GameAction> AciveActions => actions.Where(x => x.IsActive);

        protected abstract void InitializeActions();

        protected Location()
        {
            InitializeActions();
        }
    }
}
