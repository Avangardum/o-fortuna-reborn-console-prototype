﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    interface IPresenter
    {
        void ShowMessage(string message);

        void AskForActions(Location location);

        void End();
    }
}
