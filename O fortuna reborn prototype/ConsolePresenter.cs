﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace O_fortuna_reborn_prototype
{
    class ConsolePresenter : IPresenter
    {
        const int MAX_LINE_SIZE = 100;

        public void AskForActions(Location location)
        {
            while (location.AciveActions.Count() > 0)
            {
                int currentNumber = 1;
                foreach (GameAction gameAction in location.AciveActions)
                    ShowMessage($"{currentNumber++} - {gameAction.Name}");
                ShowMessage("w - ждать");
                Console.Write('>');
                bool success = false;
                while (!success)
                {
                    string input = Console.ReadLine();
                    if (input == "w")
                        return;
                    int number;
                    bool parseSuccesfull = int.TryParse(input, out number);
                    if (!parseSuccesfull)
                    {
                        ShowMessage("Некорректный ввод");
                        continue;
                    }
                    if(number > location.AciveActions.Count())
                    {
                        ShowMessage("Некорректный ввод");
                        continue;
                    }
                    success = true;
                    number--;
                    location.AciveActions.ToList()[number].Invoke();
                }
            }
        }

        public void End()
        {
            ShowMessage("Игра оконена, для выхода нажмите любую кнопку");
            Console.ReadKey(true);
        }

        public void ShowMessage(string message)
        {
            var words = message.Split(' ');
            string line = "";
            foreach (var word in words)
            {
                if (line.Length + word.Length + 1 > MAX_LINE_SIZE)
                {
                    Console.WriteLine(line);
                    line = "";
                }
                line += word;
                line += ' ';
            }
            Console.WriteLine(line);
        }
    }
}
