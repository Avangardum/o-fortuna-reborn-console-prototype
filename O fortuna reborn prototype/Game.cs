﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Game
    {
        public static readonly IPresenter Presenter = new ConsolePresenter();

        public Game()
        {
            Presenter.ShowMessage("O fortuna reborn console prototype v1.0");
            new Level1();
            Presenter.End();
        }
    }
}
