﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_3 : Location
    {
        protected override void InitializeActions()
        {
            
        }

        public Location1_3()
        {
            ShowMessage("Тобиас и Коди пришли на станцию.");
            if(Level1.Instance.TrainGone)
            {
                ShowMessage("Они опоздали.");
            }
            else
            {
                ShowMessage("Они успели и уехали из города");
                Level1.Instance.TobiasSaved = true;
            }
            Level1.Instance.Ending();
        }
    }
}
